package com.example.reactivekafkaconsumerandproducer.controller;

import com.example.reactivekafkaconsumerandproducer.service.ReactiveProducerService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "api/")
public class ProducerController {

    private ReactiveProducerService reactiveProducerService;

    public ProducerController(ReactiveProducerService reactiveProducerService) {
        this.reactiveProducerService = reactiveProducerService;
    }

    @GetMapping(value = "producer")
    public ResponseEntity<String> producer() throws JsonProcessingException {

        String json = "{ \"id\": \"Baeldung\" }";
        ObjectMapper objectMapper = new ObjectMapper();
        JsonNode convertedObject1 = objectMapper.readTree(json);

        reactiveProducerService.send(convertedObject1);

        return new ResponseEntity<>("thangnm2", HttpStatus.OK);
    }
}
