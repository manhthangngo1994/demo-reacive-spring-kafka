package com.example.reactivekafkaconsumerandproducer.service;

import com.example.reactivekafkaconsumerandproducer.dto.FakeProducerDTO;
import com.fasterxml.jackson.databind.JsonNode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.reactive.ReactiveKafkaProducerTemplate;
import org.springframework.stereotype.Service;

@Service
public class ReactiveProducerService {

    private final Logger log = LoggerFactory.getLogger(ReactiveProducerService.class);
    private final ReactiveKafkaProducerTemplate<String, JsonNode> reactiveKafkaProducerTemplate;

    @Value(value = "${FAKE_PRODUCER_DTO_TOPIC}")
    private String topic;

    public ReactiveProducerService(ReactiveKafkaProducerTemplate<String, JsonNode> reactiveKafkaProducerTemplate) {
        this.reactiveKafkaProducerTemplate = reactiveKafkaProducerTemplate;
    }

    public void send(JsonNode obj) {
        long time = System.currentTimeMillis();
        String key = String.valueOf(time);
        log.info("send to topic={}, {}={},", topic, obj.getClass().getSimpleName(), obj);
        reactiveKafkaProducerTemplate.send(topic, 2, key ,obj)
                .doOnSuccess(senderResult -> log.info("sent {} offset : {}", obj, senderResult.recordMetadata().offset()))
                .subscribe();
    }

}