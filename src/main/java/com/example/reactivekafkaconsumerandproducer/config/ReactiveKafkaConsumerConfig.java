package com.example.reactivekafkaconsumerandproducer.config;

import com.example.reactivekafkaconsumerandproducer.dto.FakeConsumerDTO;
import com.fasterxml.jackson.databind.JsonNode;
import org.apache.kafka.common.TopicPartition;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.kafka.KafkaProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.core.reactive.ReactiveKafkaConsumerTemplate;
import reactor.kafka.receiver.ReceiverOptions;
import reactor.kafka.receiver.ReceiverPartition;

import java.util.Collections;

@Configuration
public class ReactiveKafkaConsumerConfig {
    @Bean
    public ReceiverOptions<String, JsonNode> kafkaReceiverOptions(@Value(value = "${FAKE_CONSUMER_DTO_TOPIC}") String topic, KafkaProperties kafkaProperties) {
        ReceiverOptions<String, JsonNode> basicReceiverOptions = ReceiverOptions.create(kafkaProperties.buildConsumerProperties());
        return basicReceiverOptions.assignment(Collections.singleton(new TopicPartition(topic, 2)));
    }

    @Bean
    public ReactiveKafkaConsumerTemplate<String, JsonNode> reactiveKafkaConsumerTemplate(ReceiverOptions<String, JsonNode> kafkaReceiverOptions) {
        return new ReactiveKafkaConsumerTemplate<String, JsonNode>(kafkaReceiverOptions);
    }
}